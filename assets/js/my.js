/* Each page is divided in to various controllers which take care of displaying of that part of page 
 * the controller make use of myservices to perform intensive works such as ajaxcall,calculation
 * the variables inside controller will have local scope
 * once the operation is completed (i.e. sorting) the controller wil assign necessary local variable to global data 
 * the change in global data will have trickle down effect and will update any other part dependent of the global data 
 *  */

/* INIT VARS ******/

baseUrl = 'index.php?r=';



function PlanController($scope) {

    //planData object holds everything related to the plan
    //userData object holder all the user data , sorts , filters and prefrences


    //initial load from server (page refresh)
    PlanController_init($scope, server_data);
//    delete server_data
    /* In case nay update from child , process that here */
    $scope.$on("UPDATE_PLAN_DATA", function(event, data) {
        PlanController_init($scope, data);
        $scope.$broadcast('PLAN_DATA_UPDATED', data);
    });

    $scope.calculateBudget = function() {

        return $scope.user.budget;
    }

    $scope.percent = function(num, denom) {
        var percent = parseInt((num / denom) * 100);
        if (percent > 100) {
            percent = 100;
        }
        return percent;
    }

    test = function() {
        
    }

}

function PlanController_init($scope, data) {
    try {
        //be aware of shallow and deep copying of objects
        $scope.planData = new Object();
        $scope.planData = JSON.parse(JSON.stringify(data.plan_data));
        $scope.userData = new Object();
        $scope.userData = JSON.parse(JSON.stringify(data.user_data));
        //initial budget is 0 then it adds up 
        $scope.userData.suggestedBudget = 0;
//        $scope.userData.filter.mainFilter.CHEAP_TRAVEL = true;
console.log($scope.planData);

        switch ($scope.planData.currency) {
            case 'INR':
                $scope.planData.currency_symbol = 'Rs';
                break;

            default:
                $scope.planData.currency_symbol = '$';
                break;
        }

//cleanup
        delete data;
        return true;

    } catch (e) {
        return false;
        console.log(e);
    }

}

//updaet child controllers about the change 



//        $scope.safeApply = function(fn) {
//  var phase = this.$root.$$phase;
//  if(phase == '$apply' || phase == '$digest') {
//    if(fn && (typeof(fn) === 'function')) {
//      fn();
//    }
//  } else {
//    this.$apply(fn);
//  }
//};


//takes care of top filter of details page
function SearchController($scope, myService) {
    //show existing selected data in the form
    $scope.gPlace_from;
    $scope.gPlace_to;


    //search for plan as filtered by the user
    $scope.search = function() {
        var data = $scope.userData;
        var url = baseUrl + 'plan/index';
        myService.ajaxCall(url, data).then(function(result) {
            if (!result) {
                return
            }

            var planData = new Object();
            var userData = new Object();
            console.log(result);
            //Now notify parent of the change          
            $scope.$emit("UPDATE_PLAN_DATA", result);

        });
    }
}
//controles the display of naviations element and notifications
function NavController($scope) {



}
//controles the display of sidebar naviations element and notifications
function SidebarController($scope) {



}

function TravelController($scope) {
    TravelController_init($scope)
    $scope.$on("PLAN_DATA_UPDATED", function(event, data) {

        TravelController_init($scope)
    });
}
function TravelController_init($scope) {
    try {

        if (!$scope.planData.travel.routes) {
            return false;
        }

        $scope.routes = $scope.planData.travel.routes;
        var temp = $scope.planData.travel.routes;
        $scope.max = temp.slice(-1)[0].price;
        $scope.suggested = {price: $scope.routes[0].price, name: $scope.routes[0].name};
        $scope.userData.suggestedBudget = $scope.userData.suggestedBudget + parseInt($scope.suggested.price);
        return true
    } catch (e) {
        console.log(e);
        return false;
    }


}
function xTravelTableController($scope, $filter, $q, ngTableParams) {
    
    
    TravelTableController_init($scope, $filter, $q, ngTableParams)
    $scope.$on("PLAN_DATA_UPDATED", function(event, data) {
        
    TravelTableController_init($scope, $filter, $q, ngTableParams)
    });
       

}

function TravelTableController($scope, $filter, $q, ngTableParams) {

    try {

        var data = $scope.routes;
        
        $scope.$on("PLAN_DATA_UPDATED", function(event, data) {
        
    $scope.tableParams.reload();
    });
    var getData = function() {
        return $scope.routes;
    };
    $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,          // count per page
        
    }, {
        total: function () { return getData().length; }, // length of data
        getData: function($defer, params) {
            var filteredData = getData();
            var orderedData = params.sorting() ?
                                $filter('orderBy')(filteredData, params.orderBy()) :
                                filteredData;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        },
        $scope: { $data: {} }
    });

        //hack to click on sort function so that grid reloads : TODO fix using ng reload method 
        //document.getElementById('travel_table').children[2].children[0].children[0].children[0].click();
        return true;

    } catch (e) {
        console.log(e);
        return false;
    }


}

function StayController($scope) {
   StayController_init($scope)
    $scope.$on("PLAN_DATA_UPDATED", function(event, data) {

        StayController_init($scope)
    });
}
function StayController_init($scope) {
    if (!$scope.planData.stay.hotels) {
        return false;
    }

    try {
        $scope.hotels = $scope.planData.stay.hotels;

        $scope.max = $scope.hotels.slice(-1)[0].lowRate;
        $scope.suggested = {price: $scope.hotels[0].lowRate,
            name: $scope.hotels[0].name,
            currencyCode: $scope.hotels[0].rateCurrencyCode
        };

        $scope.userData.suggestedBudget = $scope.userData.suggestedBudget + parseInt($scope.suggested.price);
        return true;
    } catch (e) {
        console.log(e);
        return false;

    }
}

function xStayTableController($scope, $filter, $q, ngTableParams) {

     $scope.imgBaseUrl = 'http://images.travelnow.com';
    StayTableController_init($scope, $filter, $q, ngTableParams)
    $scope.$on("PLAN_DATA_UPDATED", function(event, data) {
    StayTableController_init($scope, $filter, $q, ngTableParams)
    });
}

function StayTableController($scope, $filter, $q, ngTableParams) {

    try {
$scope.imgBaseUrl = 'http://images.travelnow.com';
        var data = $scope.hotels;
        $scope.$on("PLAN_DATA_UPDATED", function(event, data) {
        
    $scope.tableParams.reload();
    });
    var getData = function() {
        return $scope.hotels;
    };
    $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,          // count per page
        
    }, {
        total: function () { return getData().length; }, // length of data
        getData: function($defer, params) {
            var filteredData = getData();
            var orderedData = params.sorting() ?
                                $filter('orderBy')(filteredData, params.orderBy()) :
                                filteredData;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        },
        $scope: { $data: {} }
    });
//hack to click on sort function so that grid reloads : TODO fix using ng reload method 
        //document.getElementById('stay_table').children[2].children[0].children[0].children[1].click();
        return true;

    } catch (e) {
        console.log(e);
        return false;
    }

$scope.imgUrl = function(img){
    return $scope.imgBaseUrl+img;
}
}

/**START  FOR EXPLORE DATA BLOCK ******************************/
function ExploreController($scope) {

$scope.$on("PLAN_DATA_UPDATED", function(event, data) {

       ExploreController_init($scope);
    });
ExploreController_init($scope);
}
function ExploreController_init($scope) {
//     if (!is_valid('businesses',$scope.planData.explore.places)) {
//        return;
//    }
    try {
        
        $scope.places = $scope.planData.explore.places.businesses;
window.console.log($scope.places);
        $scope.max = $scope.places.slice(-1)[0].rating;
        $scope.suggested = {rating: $scope.places[0].rating,
            name: $scope.places[0].name,
        };
return true;
    }
    catch (e) {
        console.log(e);
        $scope.places = '';
        return false;
    }




}


function xExploreTableController($scope, $filter, $q, ngTableParams) {
    
    ExploreTableController_init($scope, $filter, $q, ngTableParams)
    $scope.$on("PLAN_DATA_UPDATED", function(event, data) {
    ExploreTableController_init($scope, $filter, $q, ngTableParams)
      });
}
function ExploreTableController($scope, $filter, $q, ngTableParams) {

    try {

        var data = $scope.places;
        $scope.$on("PLAN_DATA_UPDATED", function(event, data) {
        
    $scope.tableParams.reload();
    });
    var getData = function() {
        return $scope.places;
    };
    $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,          // count per page
        
    }, {
        total: function () { return getData().length; }, // length of data
        getData: function($defer, params) {
            var filteredData = getData();
            var orderedData = params.sorting() ?
                                $filter('orderBy')(filteredData, params.orderBy()) :
                                filteredData;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        },
        $scope: { $data: {} }
    });
        

        return true;

    } catch (e) {
        console.log(e);
        return false;
    }


}


/** END FOR EXPLORE DATA BLOCK ******************************/


function MyPlanCtrl($scope) {

    $scope.gPlace_from;
    $scope.gPlace_to;


}
function MyIndexCtrl($scope, myService) {

    $scope.gPlace_from;
    $scope.gPlace_to;
    $scope.planData = new Object();
    $scope.userData = new Object();



}
function DemoCtrl($scope) {

}



//**************************************************** UTILITY CLASSES ***********************************************


//START GOOGLE PLACES AUTOCOMPLETE 

var myApp = angular.module('myApp', ['ngTable']);

myApp.directive('googleplace', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, model) {
            var options = {
                types: ['(cities)'],
                componentRestrictions: {}
            };
            var input_from = document.getElementById('input_from');
            var input_to = document.getElementById('input_to');


            scope.gPlace_from = new google.maps.places.Autocomplete(input_from, options);
            scope.gPlace_to = new google.maps.places.Autocomplete(input_to, options);
            //google maps getting from city and its lat and long data 
            google.maps.event.addListener(scope.gPlace_from, 'place_changed', function() {

                scope.$apply(function() {
                    scope.userData.from = new Object
                    scope.userData.from.name = scope.gPlace_from.getPlace().name;
                    scope.userData.from.lat = scope.gPlace_from.getPlace().geometry.location.lat();
                    scope.userData.from.lng = scope.gPlace_from.getPlace().geometry.location.lng();
                });
                console.log(scope.userData)
            });
            //google maps getting to city and its lat and long data 
            google.maps.event.addListener(scope.gPlace_to, 'place_changed', function() {

                scope.$apply(function() {
                    scope.userData.to = new Object
                    scope.userData.to.name = scope.gPlace_to.getPlace().name;
                    scope.userData.to.lat = scope.gPlace_to.getPlace().geometry.location.lat();
                    scope.userData.to.lng = scope.gPlace_to.getPlace().geometry.location.lng();
                });
            });
            console.log(scope.userData)

        }
    };
});
////myApp.factory('myService', function() {});
////// END GOOGLE PLACES AUTOCOMPLETE
////
////
////
////AJAX FACTORY 
//
myApp.factory('myService', function($http) {
    return {
        ajaxCall: function(url, data) {
            //since $http.get returns a promise,
            //and promise.then() also returns a promise
            //that resolves to whatever value is returned in it's 
            //callback argument, we can return that.
            var dataType = {datatype: 'JSON'};
            $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
//hack try to change it to better angular way 
document.getElementById('ajax_loader').style.display="block";
            // Override $http service's default transformRequest
            $http.defaults.transformRequest = [function(data)
                {
                    /**
                     * The workhorse; converts an object to x-www-form-urlencoded serialization.
                     * @param {Object} obj
                     * @return {String}
                     */
                    var param = function(obj)
                    {
                        var query = '';
                        var name, value, fullSubName, subName, subValue, innerObj, i;

                        for (name in obj)
                        {
                            value = obj[name];

                            if (value instanceof Array)
                            {
                                for (i = 0; i < value.length; ++i)
                                {
                                    subValue = value[i];
                                    fullSubName = name + '[' + i + ']';
                                    innerObj = {};
                                    innerObj[fullSubName] = subValue;
                                    query += param(innerObj) + '&';
                                }
                            }
                            else if (value instanceof Object)
                            {
                                for (subName in value)
                                {
                                    subValue = value[subName];
                                    fullSubName = name + '[' + subName + ']';
                                    innerObj = {};
                                    innerObj[fullSubName] = subValue;
                                    query += param(innerObj) + '&';
                                }
                            }
                            else if (value !== undefined && value !== null)
                            {
                                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                            }
                        }

                        return query.length ? query.substr(0, query.length - 1) : query;
                    };

                    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
                }];
            return $http({
                url: url,
                method: 'POST',
                headers: {'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest'},
                data: data
            }).then(function(result) {
document.getElementById('ajax_loader').style.display="none";
                return result.data;
            });
        }
    }
});




//END AJAX FACTORY 

function is_valid(key, item) {
// http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: FremyCompany
    // +   improved by: Onno Marsman
    // +   improved by: Rafał Kukawski
    // *     example 1: isset( undefined, true);
    // *     returns 1: false
    // *     example 2: isset( 'Kevin van Zonneveld' );
    // *     returns 2: true

    try {
        if (typeof item != 'undefined' && item != null && item != '' && !item && typeof item[key] != 'undefined' && item[key] != null && item[key] != '' && !item[key]) {
            return true;
        }
    } catch (e) {
        return false;
    }



}

      
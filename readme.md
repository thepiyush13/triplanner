#TriPlaner

The budget management planner for any trip between cities

##Features

 * Travel : cheapest route selection between locations 
 * Stay : cheapest and best suited hotel selection 
 * Connect : connects with social media friends to travel together 
 * Explore : based on social media profile and other data suggests places worth visiting during the trip


##Configuration

* Needs Yii framework 
* Needs Angular js framework 

##Installation

* Place the yii framework outside of root repository
* Place the entire repo folder inside web accessible folder
* Link up repo folder to yii framework folder by setting file path in index.php
* Set required permission,databases etc 



##Copyright
Piyush Tripathi <thepiyush13@gmail.com>.
2013-2014


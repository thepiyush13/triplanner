<?php

class PlanController extends Controller {

    /**
     * Declares class-based actions.
     */
    function init() {

//         $currency  = isset(yii::app()->params['CURRENCY']) ? yii::app()->params['CURRENCY'] : 'USD';
//        $settings = array(
//            'CURRENCY' => $currency,
//            'ROME2RIO_API_KEY' => '25CMGzLw',
//            'EXPEDIA_API_KEY'=>'gnhrd6f9x2u2zshdnm4f8sfj'
//        );
//        
        $request = Yii::app()->request;
        $session = Yii::app()->session;
        $session->open();
        $session['CURRENCY'] = isset($session['CURRENCY']) ? $session['CURRENCY'] : 'USD';
        $session['ROME2RIO_API_KEY'] = '25CMGzLw';
        $session['EXPEDIA_API_KEY'] = 'gnhrd6f9x2u2zshdnm4f8sfj';
//$this->layout = 'bootplus';
    }

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
//        $this->render('details', array('server_data' => ''));
//        return true;
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
 
  
  
        $request = Yii::app()->request;
        
       
        $plan_data = array(
            'travel' => '',
            'stay' => '',
            'explore' => '',
            'connect' => ''
        );
        $msg = array();
        //clean up the post data : sanitize and validate before using 
        //if no data is posted , just render the default view 
        $is_valid = $request->getPost('from', FALSE) && $request->getPost('to', FALSE) && $request->getPost('budget', FALSE);
        if (!$request->getIsPostRequest()) {
//            $this->layout = 'index';  
            $this->render('index');
            return true;
        } else if (!$is_valid) {
            $this->layout = 'index';
            MyUtils::setFlash('info', 'Please fill the required fields');
            $this->render('index');
            return true;
        }

        //extracting the variables 
        $from = $request->getPost('from', FALSE);
        $to = $request->getPost('to', FALSE);
        $budget = (int) $request->getPost('budget', FALSE);


        $model = new Plan;
        // getting the travel data : routes and sorted data 


        $routes = $model->get_travel_data($from, $to, $budget);
        if (isset($routes['error'])) {
            $msg[] = array('error' => $routes['data']);
        } else {
            $plan_data['travel']['routes'] = $routes;
        }


        //getting the stay data : EAN api for hotel list and their average price 
        $hotels = $model->get_stay_data($from, $to, $budget);
        if (isset($hotels['error'])) {
            $msg[] = array('error' => $hotels['data']);
        } else {
            $plan_data['stay']['hotels'] = $hotels;
        }
        
        
          //getting the places data : Yelp api for business listing  
        $type='coffee';
        $places = $model->get_explore_data($from,$to,$type,$budget);
        if (isset($places['error'])) {
            $msg[] = array('error' => $places['data']);
        } else {
            $plan_data['explore']['places'] = $places;
        }

        //getting event api for events related to the destination 
//        $events = $model->get_explore_data($from, $to, $budget);
//        $plan_data['explore']['events'] = $events;
        //getting social data 
        //render the view 

        $plan_data['currency'] = yii::app()->session['CURRENCY'];
        $user_data = array(
            'from' => $from,
            'to' => $to,
            'budget' => $budget
        );


        $server_data = array(
            'plan_data' => $plan_data,
            'user_data' => $user_data
        );


        //if request is ajax send it as json else render the page 
        if ($request->isAjaxRequest) {
            echo MyUtils::json_encode($server_data);
            return true;
        }
        MyUtils::setMultiFlash($msg);
        $this->render('details', array('server_data' => $server_data));
    }

    /**
     * This is the action to handle external exceptions.
     */

    /**
     * Displays the login page
     */
    public function actionDetails() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionTest() {
//         $model = new Plan;
//         $to['lat'] = 4300.653226;
//        $to['lng'] = -7009.38318429999998;
//        $to['name'] = 'ddddddddddddddddddddddddd';
//        $type='coffeddde';
//        $model->get_explore_data(false,$to,3000);
        $this->render('test', array('server_data' => ''));
    }

    function changeCurrency() {
        
    }

}
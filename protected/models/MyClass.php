<?php

/**
 * @package MyClass
 * @author Piyush Tripathi
 * @copyright Piyush Tripathi 2013
 * @version 1.0
 * @access private
 */

class MyClass {
    
    

    

    public function __construct($validations = array(), $mandatories = array(), $sanatations = array()) {
       
    }

    /**
     * Validates an array of items (if needed) and returns true or false
     *
     */
    public function validate($items) {
        $this->fields = $items;
        $havefailures = false;
        foreach ($items as $key => $val) {
            if ((strlen($val) == 0 || array_search($key, $this->validations) === false) && array_search($key, $this->mandatories) === false) {
                $this->corrects[] = $key;
                continue;
            }
            $result = self::validateItem($val, $this->validations[$key]);
            if ($result === false) {
                $havefailures = true;
                $this->addError($key, $this->validations[$key]);
            } else {
                $this->corrects[] = $key;
            }
        }

        return(!$havefailures);
    }

    /**
     *
     *  Adds unvalidated class to thos elements that are not validated. Removes them from classes that are.
     */
    public function getScript() {
        if (!empty($this->errors)) {
            $errors = array();
            foreach ($this->errors as $key => $val) {
                $errors[] = "'INPUT[name={$key}]'";
            }

            $output = '$$(' . implode(',', $errors) . ').addClass("unvalidated");';
            $output .= "new FormValidator().showMessage();";
        }
        if (!empty($this->corrects)) {
            $corrects = array();
            foreach ($this->corrects as $key) {
                $corrects[] = "'INPUT[name={$key}]'";
            }
            $output .= '$$(' . implode(',', $corrects) . ').removeClass("unvalidated");';
        }
        $output = "<script type='text/javascript'>{$output} </script>";
        return($output);
    }

    /**
     *
     * Sanatizes an array of items according to the $this->sanatations
     * sanatations will be standard of type string, but can also be specified.
     * For ease of use, this syntax is accepted:
     * $sanatations = array('fieldname', 'otherfieldname'=>'float');
     */
    public function sanatize($items) {
        foreach ($items as $key => $val) {
            if (array_search($key, $this->sanatations) === false && !array_key_exists($key, $this->sanatations))
                continue;
            $items[$key] = self::sanatizeItem($val, $this->validations[$key]);
        }
        return($items);
    }

    /**
     *
     * Adds an error to the errors array.
     */
    private function addError($field, $type = 'string') {
        $this->errors[$field] = $type;
    }

    /**
     *
     * Sanatize a single var according to $type.
     * Allows for static calling to allow simple sanatization
     */
    public static function sanatizeItem($var, $type) {
        $flags = NULL;
        switch ($type) {
            case 'url':
                $filter = FILTER_SANITIZE_URL;
                break;
            case 'int':
                $filter = FILTER_SANITIZE_NUMBER_INT;
                break;
            case 'float':
                $filter = FILTER_SANITIZE_NUMBER_FLOAT;
                $flags = FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND;
                break;
            case 'email':
                $var = substr($var, 0, 254);
                $filter = FILTER_SANITIZE_EMAIL;
                break;
            case 'string':
            default:
                $filter = FILTER_SANITIZE_STRING;
                $flags = FILTER_FLAG_NO_ENCODE_QUOTES;
                break;
        }
        $output = filter_var($var, $filter, $flags);
        return($output);
    }

    /**
     *
     * Validates a single var according to $type.
     * Allows for static calling to allow simple validation.
     *
     */
    public static function validateItem($var, $type) {
        if (!isset($var) || !isset($type)) {
            return FALSE;
        }
        if (array_key_exists($type, self::$regexes)) {
            $returnval = filter_var($var, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => '!' . self::$regexes[$type] . '!i'))) !== false;
            return($returnval);
        } else if ($type == 'string') {
            $result = is_string($var) ? TRUE : FALSE;
            return $result;
        } else if ($type == 'array') {
            $result = is_array($var) ? TRUE : FALSE;
            return $result;
        } else if ($type == 'multi_array') {
            $result = (count($var) != count($var, 1)) ? TRUE : FALSE;
            return $result;
        } else {
            $filter = false;
            switch ($type) {
                case 'email':
                    $var = substr($var, 0, 254);
                    $filter = FILTER_VALIDATE_EMAIL;
                    break;
                case 'int':
                    $filter = FILTER_VALIDATE_INT;
                    break;
                case 'boolean':
                    $filter = FILTER_VALIDATE_BOOLEAN;
                    break;
                case 'ip':
                    $filter = FILTER_VALIDATE_IP;
                    break;
                case 'url':
                    $filter = FILTER_VALIDATE_URL;
                    break;
            }
            return ($filter === false) ? false : filter_var($var, $filter) !== false ? true : false;
        }
    }

    /**
     *
     * Runs Sql query and returns the output 
     * Uses prepared statements for Sql injection protection
     * $sql = 'SELECT * from tbl where id=:id' ,$bindings= array(':id'=>$your_id)
     */
    public static function runSql($sql, $bindings = null) {
        if (!self::validateItem($sql, 'string')) {
            return FALSE;
        }
        if ($bindings) {
            $result = Yii::app()->db->createCommand($sql)->bindValues($bindings)->queryAll();
        } else {
            $result = Yii::app()->db->createCommand($sql)->queryAll();
        }

        return $result;
    }

    /**
     * Inserts Multiple items or single item to the table specified 
     * Uses prepared statements for Sql injection protection
     * Returns array of inserted items ids False if any error occured
     * insertTable('table_name',array('col1'=>'val1','col2'=>'val2'))
     */
    public function insertTable($table_name, $data, $pk) {
        if (!self::validateItem($table_name, 'string') || !self::validateItem($data, 'array')) {
            return FALSE;
        }
        $result = array();
        $table = Yii::app()->db->createCommand();
        //begin transaction : if any row insertion fails, all the changed will be rolled back
        $transaction = Yii::app()->db->beginTransaction();
        try {


            if (self::validateItem($data, 'multi_array')) {
                foreach ($data as $row) {
                    $table->insert($table_name, $row);
                    $result[] = Yii::app()->db->lastInsertID;
                }
            } else {

                $table->insert($table_name, $data);
                $result[] = Yii::app()->db->lastInsertID;
            }

            $this->is_error = FALSE;
            $transaction->commit();
        } catch (Exception $e) {
            $this->is_error = TRUE;
            $this->error_msg[] = $e->getMessage();
            $transaction->rollback();
            $result = FALSE;
        }

        return $result;
    }

    /**
     * Updates Multiple items or single item to the table specified 
     * If item is found , it updates the values , if not inserts a new value
     * Returns all inserted and updated ids in associative array
     * insertTable('table_name',array('col1'=>'val1','col2'=>'val2'))
     */
    public function updateTable($tbl_mdl, $data) {
        if (!self::validateItem($tbl_mdl, 'string') || !self::validateItem($data, 'array')) {
            return FALSE;
        }
        $result = array();
        $table = Yii::app()->db->createCommand();
        /* @TODO
         * Get all data base rows : db list
         * Get all modified list 
         * find rows need deletion : not in modified list
         * find rows to be added : not in db list
         * find rows to be updated : in both array
         * delete -> update ->insert
         */
        //begin transaction : if any row insertion fails, all the changed will be rolled back
        $transaction = Yii::app()->db->beginTransaction();
        try {


            if (self::validateItem($data, 'multi_array')) {
                foreach ($data as $row) {
                    $pk_val = $row[$pk];
                    $record_exist = self::runSql('SELECT * from :table where :pk_column = :pk_value', array(':table' => $tbl_mdl, ':pk_column' => $pk, ':pk_value' => $pk_val));
                    if ($record_exist) {
                        
                    } else {
                        
                    }
                    $table->insert($tbl_mdl, $row);
                    $result[] = Yii::app()->db->lastInsertID;
                }
            } else {

                $table->insert($tbl_mdl, $data);
                $result[] = Yii::app()->db->lastInsertID;
            }

            $this->is_error = FALSE;
            $transaction->commit();
        } catch (Exception $e) {
            $this->is_error = TRUE;
            $this->error_msg[] = $e->getMessage();
            $transaction->rollback();
        }

        return $result;
    }

    public static function display($data) {
        echo '<pre>';
        echo print_r($data);
        echo '<pre>';
    }

    

    function test($data) {
//        array_sl
    }

    

}

?>
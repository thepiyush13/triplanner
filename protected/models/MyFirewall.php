<?php

/**

 * This is a class for Accessing user data in a safe way 
 *
 * @author     Piyush Tripathi <thepiyush13@gmail.com>
 * @copyright  Piyush Tripathi
 * @version    1.0
 */

/**
 *  This is a class for Accessing user data in a safe way
 *  xss prevention
 *  typecasting 
 */
class MyFirewall {

   

    /**
     *  Constructor 
     *  @param resource $result 
     *  @param resource $connx 
     *  @return void 
     */
    public function __construct() {
        
    }

    /**
     *  Clean input array to prevent xss attacks 
     *  @param mixed $input the input variable
     *  @param mixed $type
     *  can be of type : "boolean","integer","double" ,"string","array","object","resource","NULL" 
     *  @return mixed cleaned input typecasted 
     */
    public static function cleanInput($input = NULL,$type=NULL) {
        if ( !$input) {
            return false;
        }
        $parser = new CHtmlPurifier(); //create instance of CHtmlPurifier
        $clean_input = $parser->purify($input); //we purify the $user_input
        if($type!=NULL){
           settype($clean_input, $type);
        }
        return $clean_input;
    }

// end 

    /**
     *  Clean input array to prevent xss attacks 
     *  @param mixed array $input_array 
     * i.e. array('param1'=>'value1','param2'=>'value2',...) 
     *  @return cleaned input 
     * i.e. array('param1'=>'clean_value1','param2'=>'clean_value2',...) 
     */
    public static function cleanInputArray($input_array = NULL) {
        if (!$input || !is_array($input_array)) {
            return false;
        }
        $result = array();
        foreach ($input_array as $key => $value) {
            $result[$key] = self::cleanInput($value);
            
        }
        return $result;
    }

// end 

    

}

// end class MyFirewall
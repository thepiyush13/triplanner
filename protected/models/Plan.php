<?php

/**

 * This is a class for planning of the entire travel,hotel and events 
 *
 * @author     Piyush Tripathi <thepiyush13@gmail.com>
 * @copyright  Piyush Tripathi
 * @version    1.0
 */
class Plan {

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    function get_plan_data($from, $to, $budget) {
       
//        $xmlstr = 'http://evaluate.rome2rio.com/api/1.2/xml/Search?key=25CMGzLw&oName=London&dName=Paris';
//        $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
//        $url = 'http://evaluate.rome2rio.com/api/1.2/xml/Search?key=25CMGzLw&oName=London&dName=Paris';
        $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
        $url = '/var/www/data/rome2rio_example.xml';
        $xml = file_get_contents($url, false, $context);
//        file_put_contents('rome2rio_example.xml', $xml);
        $xml = simplexml_load_string($xml);
        $myclass = new MyClass;
        $data = $myclass->objectToArray($xml);
        $routes = $myclass->seekKey($data, 'Route');
        //cleaning up the array
        $replace_from = '@attributes';
        $replace_to = 'attributes';
        $result = $myclass->renameArrayKeys($routes, $replace_from, $replace_to);
        //flattening the array for better handelling 
        $output = array();
        
        foreach ($result as $key => $value) {
            $k = 'Route-' . ($key + 1);
            $v = array_merge($value['attributes'], $value['IndicativePrice']['attributes']);
            $output[$k] = $v;
        }

        $output = array();
        $output = $myclass->test($data['Route']);
        //$myclass->display($output);
//        die('test');
        //getting the sorted data 

        return ($output);
    }

    /**

     * @param lat $from coordinate for from city
     * @param long $to the coodrinate of to city
     * @return type  md array of travel : routes and other data 
     * @throws return false 
     * @access public
     */
    public function get_travel_data($from, $to, $budget) {
        //init

        $myclass = new MyClass;
        $is_valid = FALSE;

        //sanitize and validate the data
//        $is_valid = $myclass->validateItem($from, 'string') && $myclass->validateItem($to, 'string') && $myclass->validateItem($budget, 'int');
//        if (!$is_valid) {
//            return false;
//        }
        //helper function calling starts 
        $routes = $this->get_plan_routes($from, $to);
        //getting the routes for the plan 
        //getting other dara 
        //creating final array 
        //outputtin the data or any error
        return $routes;
    }

    /**

     * @param lat $from coordinate for from city
     * @param long $to the coodrinate of to city
     * @return type  md array of Hotels listing and other data 
     * @throws return false 
     * @access public
     */
    public function get_stay_data($from, $to, $budget) {
        //init
        $myclass = new MyClass;
        $is_valid = FALSE;
        //sanitize and validate the data
//        $is_valid = $myclass->validateItem($from, 'string') && $myclass->validateItem($to, 'string') && $myclass->validateItem($budget, 'int');
//        if (!$is_valid) {
//            return false;
//        }
        //helper function calling starts 
        $hotels = $this->get_plan_hotels($from, $to, $budget);
        //getting the routes for the plan 
        //getting other dara 
        //creating final array 
        //outputtin the data or any error
        return $hotels;
    }

    /**

     * @param lat $from coordinate for from city
     * @param long $to the coodrinate of to city
     * @return type  md array of places listing and other data 
     * @throws return false 
     * @access public
     */
    public function get_explore_data($from, $to, $budget) {
        //init
        
        $is_valid = FALSE;
        //sanitize and validate the data
//        $is_valid = $myclass->validateItem($from, 'string') && $myclass->validateItem($to, 'string') && $myclass->validateItem($budget, 'int');
//        if (!$is_valid) {
//            return false;
//        }
        //helper function calling starts 
        $type = 'coffee';
        $places = $this->get_plan_places($from, $to, $budget, $type);
        //getting the routes for the plan 
        //getting other dara 
        //creating final array 
        //outputtin the data or any error
        return $places;
    }

    /**

     * @param lat  from lattitutde
     * @param long  to langitutde
     * @return type  mdarray or false
     * @throws Exception_description
     * @access private
     */
    private function get_plan_routes($from, $to) {
        //init
        $myarray = new MyArray;
        $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
        $url = '/var/www/data/rome2rio_example.xml';

        try {
            //getting the api xml 
            //        $xmlstr = 'http://evaluate.rome2rio.com/api/1.2/xml/Search?key=25CMGzLw&oName=London&dName=Paris

            $route_xml_data = array(
                'key' => Yii::app()->session['ROME2RIO_API_KEY'],
                'oPos' => $from['lat'] . ',' . $from['lng'],
                'dPos' => $to['lat'] . ',' . $to['lng'],
                'currency' => Yii::app()->session['CURRENCY']
            );
            $route_xml_url = 'http://evaluate.rome2rio.com/api/1.2/xml/Search?' .
                    http_build_query($route_xml_data, "&amp;");
//             die($route_xml_url);
            $data = $this->file_read_contents($route_xml_url, $context);

            //if returned data is invalid then stop
            if (isset($data['EanWsError']))
                throw new Exception("Invalid data returned from Routes,Try again in few moments ");
            //get the route part of the array 
            $routes = $data['Route'];
            //cleanup the array for js use : replace @attributes to attributes 
            $replace_from = '@attributes';
            $replace_to = 'attributes';
            $result = $myarray->renameArrayKeys($routes, $replace_from, $replace_to);
//            die(print_r($result));
            //flattening the array for better handelling 
            //TODO : this functions breaks a lot , fix it for better graceful handelling of erros 
            $output = array();
if (!isset($result)||!is_array($result)) 
                throw new Exception("Invalid data returned from Routes,Try again in few moments ");
            foreach ($result as $key => $value) {
//                $k = 'Route-' . ($key + 1);
                if(!is_array($value['attributes']) || !is_array($value['IndicativePrice']['attributes'])){
                    throw new Exception("Invalid data returned from Routes,Try again in few moments ");
                }
                $v = array_merge($value['attributes'], $value['IndicativePrice']['attributes']);
                $output[$key] = $v;
            }

//            $output = $myarray->createInstance($result);
//            $flat_array = $output->filter('/n/attributes/name');
//MyUtils::display($flat_array);
            $column = 'price';
            $output_sorted = $myarray->sortMultiArray($output, $column, false, 'asc');
            //return array 
            return $output_sorted;
        } catch (Exception $exc) {
            $error = array(
                'error' => TRUE,
                'data' => $exc->getMessage()
            );
            return $error;
        }
    }

    /**

     * @param lat  from lattitutde
     * @param long  to langitutde
     * @param int  $budget
     * @return type  mdarray or false
     * @throws Exception_description
     * @access private
     */
    private function get_plan_hotels($from, $to, $budget) {
        //init

        ini_set('display_errors', 1);
        $myarray = new MyArray;

        $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));




        $hotel_xml_data = array(
            'apiKey' => Yii::app()->session['EXPEDIA_API_KEY'],
            'latitude' => $to['lat'],
            'longitude' => $to['lng'],
            'currency' => Yii::app()->session['CURRENCY'],
            'sort' => 'PRICE'
        );
        $hotel_xml_url = 'https://api.eancdn.com/ean-services/rs/hotel/v3/list?cid=55505&minorRev=99&' .
                http_build_query($hotel_xml_data, "&amp;");

//        die($hotel_xml_url);
        $url = '/var/www/data/expedia_hotel_list.xml'; // $base_url.rawurlencode($url_xml);//
        //die(print_r($base_url));
        try {
            //getting the api xml 


            $data = $this->file_read_contents($hotel_xml_url, $context);

            //if returned data is invalid then stop
            if (isset($data['EanWsError']))
                throw new Exception("Invalid data returned from Routes,Try again in few moments ");

            //get the route part of the array 
            $HotelSummary = $data['HotelList']['HotelSummary'];
            //cleanup the array for js use : replace @attributes to attributes 
            $replace_from = '@attributes';
            $replace_to = 'attributes';
            $result = $myarray->renameArrayKeys($HotelSummary, $replace_from, $replace_to);
            //flattening the array for better handelling 
            $output = array();
            foreach ($result as $key => $value) {
                if ($value['highRate'] != 0.0 || $value['lowRate'] != 0.0) {
                    $output[$key] = $value;
                }
            }

            $column = 'lowRate';
            $output_sorted = $myarray->sortMultiArray($output, $column, false, 'asc');
            $output_filtered = array_slice($output_sorted, 0, 10);
            //return array 
            return $output_filtered;
        } catch (Exception $exc) {
            $error = array(
                'error' => TRUE,
                'data' => $exc->getMessage()
            );
            return $error;
        }
    }

    /**

     * @param lat  from lattitutde
     * @param long  to langitutde
     * @return type  mdarray or false
     * @throws Exception_description
     * @access private
     */
    private function get_plan_places($from, $to, $budget, $type) {
        //init
        
        
        $url = '/var/www/data/rome2rio_example.xml';

        try {
            $places_xml_data = array(
                'term' => $type,
                'location' => $to['name'],
                'cll' => $to['lat'] . ',' . $to['lng'],
            );
            $places_xml_url = 'http://api.yelp.com/v2/search?' .
                    http_build_query($places_xml_data, "&amp;");

            $unsigned_url = $places_xml_url;
//return  $unsigned_url;
// Set your keys here
            $consumer_key = "eO_2OHhXRcYHegYGv4Ui_g";
            $consumer_secret = "NDTCjv7DWSQ_h1TerQ4qe4oQNII";
            $token = "6PNaKiELaRyd8pmW1cLjQGZTUaaPkQes";
            $token_secret = "BQO4dG0z5yYeYhROePY9MZuMOTU";

// Token object built using the OAuth library
            $token = new OAuthToken($token, $token_secret);

// Consumer object built using the OAuth library
            $consumer = new OAuthConsumer($consumer_key, $consumer_secret);

// Yelp uses HMAC SHA1 encoding
            $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

// Build OAuth Request using the OAuth PHP library. Uses the consumer and token object created above.
            $oauthrequest = OAuthRequest::from_consumer_and_token($consumer, $token, 'GET', $unsigned_url);

// Sign the request
            $oauthrequest->sign_request($signature_method, $consumer, $token);

// Get the signed URL
            $signed_url = $oauthrequest->to_url();

// Send Yelp API Call
//$ch = curl_init($signed_url);
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($ch, CURLOPT_HEADER, 0);
//$data = curl_exec($ch); // Yelp response
//curl_close($ch);
           
$data = @file_get_contents($signed_url, FALSE);

// Handle Yelp response data

            $response_object = json_decode($data);
             $myarray = new MyArray;
                $response_array = $myarray->objectToArray($response_object);
          
            if(!$response_array || isset($response_array['error'])){
    throw new Exception('Could not get places for this city,Please try another city');
}
            
// Print it for debugging

            return $response_array;
        } catch (Exception $exc) {
            $error = array(
                'error' => TRUE,
                'data' => $exc->getMessage()
            );
            
            return $error;
        }
    }

    function get_yelp_places($from, $to, $type, $budget) {
        // For example, request business with id 'the-waterboy-sacramento'
//$unsigned_url = "http://api.yelp.com/v2/business/the-waterboy-sacramento";
// For examaple, search for 'tacos' in 'sf'
//        $to['lat'] = 43.653226;
//        $to['lng'] = -79.38318429999998;
//        $to['name'] = 'Toronto';
//        $type='coffee';
        $places_xml_data = array(
            'term' => $type,
            'location' => $to['name'],
            'cll' => $to['lat'] . ',' . $to['lng'],
        );
        $places_xml_url = 'http://api.yelp.com/v2/search?' .
                http_build_query($places_xml_data, "&amp;");

        $unsigned_url = $places_xml_url;

// Set your keys here
        $consumer_key = "eO_2OHhXRcYHegYGv4Ui_g";
        $consumer_secret = "NDTCjv7DWSQ_h1TerQ4qe4oQNII";
        $token = "6PNaKiELaRyd8pmW1cLjQGZTUaaPkQes";
        $token_secret = "BQO4dG0z5yYeYhROePY9MZuMOTU";

// Token object built using the OAuth library
        $token = new OAuthToken($token, $token_secret);

// Consumer object built using the OAuth library
        $consumer = new OAuthConsumer($consumer_key, $consumer_secret);

// Yelp uses HMAC SHA1 encoding
        $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

// Build OAuth Request using the OAuth PHP library. Uses the consumer and token object created above.
        $oauthrequest = OAuthRequest::from_consumer_and_token($consumer, $token, 'GET', $unsigned_url);

// Sign the request
        $oauthrequest->sign_request($signature_method, $consumer, $token);

// Get the signed URL
        $signed_url = $oauthrequest->to_url();

// Send Yelp API Call
//$ch = curl_init($signed_url);
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($ch, CURLOPT_HEADER, 0);
//$data = curl_exec($ch); // Yelp response
//curl_close($ch);
        $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
        $data = $this->file_read_contents($signed_url, $context);
//$data = file_get_contents($signed_url, FALSE);
// Handle Yelp response data
        $response = json_decode($data);
        MyUtils::display($response);
// Print it for debugging
        return $response;
    }

    /**

     * @param string path to file 
     * @param string $context type of file to read 
     * @return file contents array   
     * @throws Exception_description
     * @access public
     */
    private function file_read_contents($path, $context) {

        try {
            $str = @file_get_contents($path, FALSE, $context);

            if ($str === FALSE) {

                throw new Exception("Unable to get  data from routes,Try again in few moments ");
                return false;
            } else {

                $xml = simplexml_load_string($str);

                //converting all the objects to array for better handelling 
                $myarray = new MyArray;
                $data = $myarray->objectToArray($xml);
                return $data;
            }
        } catch (Exception $exc) {
            $exc->getTraceAsString();
            return FALSE;
        }
    }

}

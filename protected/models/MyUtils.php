<?php

/**

 * This is a class for utitlity functions  
 *
 * @author     Piyush Tripathi <thepiyush13@gmail.com>
 * @copyright  Piyush Tripathi
 * @version    1.0
 */

/**
 *  This is a class for utility functions
 *  
 */
class MyUtils  {

    static function json_encode($data){
        if(!$data){
            return FALSE;
        }
        header('Content-type: application/json');  
        return CJSON::encode($data);
    }
    
    static function json_decode($data){
        if(!$data){
            return FALSE;
        }
        return CJSON::decode($data);
    }
    
    public static function display($data,$die=TRUE) {
        echo '<h1>START DISPLAY</h1><pre>';
        echo CVarDumper::dump($data);
        echo '</pre><h1>END DISPLAY</h1>';
        
        if($die){
            die('--');
        }
    } 
        static function setMultiFlash($flashArray) {
//        if (!self::validateItem($flashArray, 'multi-array')) {
//            return false;
//        }
        foreach ($flashArray as  $key=>$value) {
            foreach ($value as $k => $v) {
                Yii::app()->user->setFlash($key, array($k,$v));
                
            }
        }
        return true;
    }
    static function setFlash($key,$message) {
       Yii::app()->user->setFlash($key,$message);        
    }
    
    static function readRawData() {
        // if type is post or get 
        return (file('php://input'));
          
    }
    
    
    
    
   
   
}

// end class 

<?php

/**

 * This is a class for various PHP utilities  
 *
 * @author     Piyush Tripathi <thepiyush13@gmail.com>
 * @copyright  Piyush Tripathi
 * @version    1.0
 */

/**
 *  This is a class for various PHP utilities  
 *  
 */


class MyArray extends arrayXpathFactory {

   /* class extended for usability 
    * Main functionality  is provided by  class below this class
    * This class is just a wrapper  , incase of any updates , just update the below mentioned class.
    */

    /**
* sorts a multidimensional array according to columns
* @param mixed  $data multidimensional array array(k=>v(array(k=>v)),k2=>v2....);
* @param mixed $column columns to sort array(k=>v,k2=>v2)
* @param string $primary_key  unique key to identify array (i.e. id )(optional )
* @param mixed $sort_type  asc for ascending desc for descinding
* @return mixed  sortedarray 
* @throws Exception_description
* @access public
*/

       public static function sortMultiArray($data, $column, $primary_key = false, $sort_type) {
        // Create a new CSort object
        $sort = new CSort();

        // One attribute for each column of data
        $sort->attributes = array($column);
//        // Set the default order

        $sort_order = ($sort_type == 'asc') ? CSort::SORT_ASC : CSort::SORT_DESC;

        $sort->defaultOrder = array($column => $sort_order);



        // Create data provider for the Grid View
        $reportDataProvider = new CArrayDataProvider($data, array(
            'id' => $primary_key,
            'sort' => $sort,
            'pagination' => false
        ));
        return $reportDataProvider->getData();
    }
    
      /**
* converst any depth object to multidimensional array
* @param mixed  $object object array
* @return mixed  array of objects 
* @throws Exception_description
* @access public
*/
    
       public  function objectToArray($object) {
        if (!is_object($object) && !is_array($object)) {
            return $object;
        }
        if (is_object($object)) {
            $object = (array) $object;
        }
        return array_map(array($this, 'objectToArray'), $object);
    }

    /**
     * Finds value of a key in multidimensional array

* @param mixed  $haystack the muldimensional array 
* @param string $needle the key to match (not value)
* @return mixed  matched value 
* @throws Exception_description
* @access public
*/

       public static function seekKey($haystack, $needle) {
        foreach ($haystack as $key => $value) {
            if ($key == $needle) {
                $this->output = $value;
            } elseif (is_array($value)) {

                $this->output = $this->seekKey($value, $needle);
            }
        }
        return $this->output;
    }

       /**
* renames a key in multidimensional array
* @param mixed  $haystack the muldimensional array 
* @param string $needle the key to match (not value)
* @return mixed  matched value 
* @throws Exception_description
* @access public
*/
       public  function renameArrayKeys($oldArr, $replace_from, $replace_to) {

        $copyArr = $oldArr;

        if (is_array($oldArr) && count($oldArr)) {
            foreach ($oldArr as $k => $v) {

                unset($copyArr[$k]); // removes old entries
                $newKey = str_replace($replace_from, $replace_to, $k);

                if (is_array($v)) {
                    $copyArr[$newKey] = $this->renameArrayKeys($v, $replace_from, $replace_to);
                } else {
                    $copyArr[$newKey] = $v;
                }
            }
            return $copyArr;
        }
    }

       /**
* sorts according to a key in multidimensional array
* @param mixed  $haystack the muldimensional array 
* @param string $needle the key to match (not value)
* @return mixed  matched value 
* @throws Exception_description
* @access public
*/
       public static function sortMultiArrayByKeyValue($data, $sortKey, $sort_flags = SORT_ASC) {
        if (empty($data) or empty($sortKey))
            return $data;

        $ordered = array();
        foreach ($data as $key => $value)
            $ordered[$value[$sortKey]] = $value;

        ksort($ordered, $sort_flags);

        return array_values($ordered); // array_values() added for identical result with multisort*
    }

    /**
* Mass delete multidimensional array keys 
     * @param type $data the array
     * @param type $value the value to be macthed
     * @return type  array with deleted keys whose value matches $value
     * @throws Exception_description
     * @access public
     */
       public static function deleteArrayKeys($data, $value, $strict_checking) {
        foreach (array_keys($data, $value, $strict_checking) as $key) {
            unset($data[$key]);
        }
        return $data;
    }
    
}

// end class MyValidation






/****************************************** EXTERNAL INCLUDED CLASSES *****************************************************
 
 * 
 * * /*******************************START ARRAY MANIPULATION CLASS*********************************************************
 *  PROVIDES ARRAY MANIPULATION USING XPATH SYNTAX
 */


//EXAMPLE USES 
//$testData = array(
//        'manufacturer' => array(
//                '1' => array(
//                        'name' => 'Ford',
//                        'models' => array(
//                                '1' => array(
//                                        'model-name' => 'Fiesta',
//                                        'model-type' => 'Car',
//                                ),
//                                '2' => array(
//                                        'model-name' => 'Transit',
//                                        'model-type' => 'Van',
//                                ),
//                        ),
//                ),
//                '2' => array(
//                        'name' => 'Volkswagen',
//                        'models' => array(
//                                '1' => array(
//                                        'model-name' => 'Golf',
//                                        'model-type' => 'Car',
//                                ),
//                        ),
//                ),
//        ),
//);
//
//
//$fact = new arrayXpathFactory();
//$inst = $fact->createInstance($testData);
//$arrFiltered = $inst->filter('manufacturer/*/models/*/model-type');
//echo '<pre>';
//print_r( $arrFiltered );
//echo '</pre>';
//
//$arrFiltered2 = $inst->filter('manufacturer/*/name');
//echo '<pre>';
//print_r( $arrFiltered2 );
//echo '</pre>';


/*
 * Copyright 2012 Karl Payne (www.karlpayne.co.uk)
 *
 * This file is part of KP-ARRAY-XPATH
 *
 * KP-Validation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * KP-Validation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KP-Validation.  If not, see <http://www.gnu.org/licenses/>.
 */



class arrayXpathFactory {

        public function createInstance($data){
                return new arrayXpath($data);
        }

}

/*
 * Copyright 2012 Karl Payne (www.karlpayne.co.uk)
 *
 * This file is part of KP-ARRAY-XPATH
 *
 * KP-Validation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * KP-Validation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KP-Validation.  If not, see <http://www.gnu.org/licenses/>.
 */



class arrayXpath {

        protected $pathStack = array();
        protected $arrData;
        public $arrPaths;

        public  function __construct(array $arrData) {
                $this->arrData = $arrData;
                $this->flatten($arrData);
        }

        protected function findValue($path){

                $arrPath = explode('/', $path);
                $retVal = $this->arrData;
                foreach($arrPath as $key){
                        if(isset($retVal[$key])){
                                $retVal = $retVal[$key];
                        } else {
                                return FALSE;
                        }
                }
                return $retVal;

        }

        public function filter($xPath = ''){

                // convert the xpath into a regex
                $pattern = $xPath;
                $pattern = str_replace('/', '\/', $pattern);
                $pattern = str_replace('*', '(.*?)', $pattern);                
                $pattern = '/^'.$pattern.'\b/';

                $filteredPaths = array();

                foreach($this->arrPaths as $value){
                        if( preg_match($pattern, $value) ){
                                $filteredPaths[$value] = $this->findValue($value);
                        }
                }

                return $filteredPaths;

        }

        protected function flatten($arrData, $realStack = array()){

                // take the first item in the stack
                $currentKey = array_shift($this->pathStack);

                foreach($arrData as $key => $value){

                        // put the real key into the realStack
                        array_push($realStack, $key);
                        // and build a real path from it
                        $realPath = implode('/',$realStack);

                        if(is_array($value)){
                                // recurse
                                $this->arrPaths[] = $realPath;

                                // recurse into the new array
                                $this->flatten($value, $realStack);

                        } else {
                                // do nothing, we want to keep this value
                                $this->arrPaths[] = $realPath;
                        }

                        // take the current key off the stack
                        array_pop($realStack);

                }
                return $arrData;
        }

}

 /*******************************END  ARRAY MANIPULATION CLASS*********************************************************
  * 
  */
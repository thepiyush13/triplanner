
<!DOCTYPE html>
<html lang="en"   >
   <head>
       <?php  $base_url =  Yii::app()->request->baseUrl;   ?>
      <meta charset="utf-8">
      <title>Smart Travel Planner</title>
      <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">

      <!-- Le styles -->
      <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
      <link href="assets/css/bootplus.css" rel="stylesheet">
      <style type="text/css">
      body {
        padding-top: 46px;
        padding-bottom: 40px;
      }
       .hero-unit {
          background: #00001C url(assets/img/cover4.jpg) no-repeat top left;
       }
       .hero-unit h1 {color: #FFF}
       .hero-unit p {color: #F5F5F5}
      </style>
      <link href="<?php echo $base_url;  ?>/assets/bootplus/css/bootplus-responsive.css" rel="stylesheet">
       
       <link href="<?php echo $base_url;  ?>/assets/css/ng-table.css" rel="stylesheet">
  <script src="<?php echo $base_url;  ?>/assets/js/angular.min.js"></script>
  <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
      <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
      <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <![endif]-->

      <!-- Fav and touch icons -->
<!--      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="assets/ico/favicon.png">
                                   -->
                                   
                                   <link href="<?php echo $base_url;  ?>/assets/css/my.css" rel="stylesheet">
  </head>

  <body>

      <div class="navbar navbar-fixed-top">
         <div class="navbar-inner">
           <div class="container">
             <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
             </button>
             <a class="brand" href="index.php?r=plan">TriPlaner</a>
             <div class="nav-collapse collapse">
               <ul class="nav">
                 <li class="active"><a href="index.php?r=plan">Home</a></li>
                 <li><a href="#about">Team</a></li>
                 <li><a href="#contact">Contact</a></li>
                 <li class="dropdown">
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown">More <b class="caret"></b></a>
                   <ul class="dropdown-menu">
                     <li><a href="#">Help</a></li>
                     <li><a href="#">Features</a></li>
                     <li><a href="#">Story</a></li>
                     <li class="divider"></li>
                     <li class="nav-header">Nav header</li>
                     <li><a href="#">Separated link</a></li>
                     <li><a href="#">One more separated link</a></li>
                   </ul>
                 </li>
               </ul>
               <form class="navbar-form pull-right">
                 <input class="span2" type="text" placeholder="Email">
                 <input class="span2" type="password" placeholder="Password">
                 <button type="submit" class="btn btn-primary">Sign in</button>
               </form>
             </div><!--/.nav-collapse -->
           </div>
         </div>
       </div>
        
      
      <?php echo $content; ?>

      
      <?php  include_once('footer.php') ?>

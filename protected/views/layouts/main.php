<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" ng-app id="ng-app" >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
        
        
        <?php  $base_url =  Yii::app()->request->baseUrl;   ?>
       <?php  $access_url = 'index.php?r=' ?>
        
        <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard - SB Admin</title>

    
    <!-- Page Specific CSS -->
    <!--<link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">-->

    <!-- Le styles -->
      <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
      <link href="<?php echo $base_url;  ?>/assets/css/bootstrap.css" rel="stylesheet">
           <link href="<?php echo $base_url;  ?>/assets/css/sb-admin.css" rel="stylesheet">
          <link href="<?php echo $base_url;  ?>/assets/css/font-awesome.min.css" rel="stylesheet">
          <!-- Bootstrap core CSS -->
          <link href="<?php echo $base_url;  ?>/assets/css/my.css" rel="stylesheet">
              <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">


    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo $base_url;  ?>/assets/js/html5shiv.js"></script>
    <![endif]-->

   
</head>

<body>

<div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo $base_url  ?>">TravelPlan</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <?php //  include('sidenav.php') ?>
            
          <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown messages-dropdown">
               
              <form name="currency" action="<?php echo $access_url;  ?>site/changeCurrency" method="POST">
                <div class="form-group">
    <select class="btn inline" name="currency">
        <option>Select Currency</option>
  <option value='USD'>$</option>
  <option value='INR'>Rs</option>
</select>
                    <button type="submit" class="btn btn-default">Select</button>
  </div>
  
  
            </form>
             
            </li>
            <li class="dropdown alerts-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> Alerts <span class="badge">3</span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Default <span class="label label-default">Default</span></a></li>
                <li><a href="#">Primary <span class="label label-primary">Primary</span></a></li>
                <li><a href="#">Success <span class="label label-success">Success</span></a></li>
                <li><a href="#">Info <span class="label label-info">Info</span></a></li>
                <li><a href="#">Warning <span class="label label-warning">Warning</span></a></li>
                <li><a href="#">Danger <span class="label label-danger">Danger</span></a></li>
                <li class="divider"></li>
                <li><a href="#">View All</a></li>
              </ul>
            </li>
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                <li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>
                <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                <li class="divider"></li>
                <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>
      <div id="page-wrapper">

    

	<?php echo $content; ?>

	<!-- Le javascript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <!--<script src="<?php echo $base_url;  ?>/assets/js/jquery.js"></script>-->
        <script src="<?php echo $base_url;  ?>/assets/js/underscore.js"></script>
       <script src="<?php echo $base_url;  ?>/assets/js/angular.min.js"></script>
      <script src="<?php echo $base_url;  ?>/assets/js/my.js"></script>
     
      
<!--      <script src="<?php echo $base_url;  ?>/assets/js/bootstrap-transition.js"></script>
      <script src="<?php echo $base_url;  ?>/assets/js/bootstrap-alert.js"></script>
      <script src="<?php echo $base_url;  ?>/assets/js/bootstrap-modal.js"></script>
      <script src="<?php echo $base_url;  ?>/assets/js/bootstrap-dropdown.js"></script>
      <script src="<?php echo $base_url;  ?>/assets/js/bootstrap-scrollspy.js"></script>
      <script src="<?php echo $base_url;  ?>/assets/js/bootstrap-tab.js"></script>
      <script src="<?php echo $base_url;  ?>/assets/js/bootstrap-tooltip.js"></script>
      <script src="<?php echo $base_url;  ?>/assets/js/bootstrap-popover.js"></script>
      <script src="<?php echo $base_url;  ?>/assets/js/bootstrap-button.js"></script>
      <script src="<?php echo $base_url;  ?>/assets/js/bootstrap-collapse.js"></script>
      <script src="<?php echo $base_url;  ?>/assets/js/bootstrap-carousel.js"></script>
      <script src="<?php echo $base_url;  ?>/assets/js/bootstrap-typeahead.js"></script>-->
      
      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->



   </body>
</html>

<?php $access_url = 'index.php?r=' ?>
<script type='text/javascript'>
<?php
$php_array = $server_data;
$js_array = json_encode($php_array,JSON_NUMERIC_CHECK);
echo "server_data = ". $js_array . ";\n";
?>
   // console.log(server_data);
</script>
<?php
$flashMessages = Yii::app()->user->getFlashes();
if ($flashMessages) {
    echo '<ul class="flashes">';
    foreach($flashMessages as $key => $message) {
        echo '<li><div class="alert alert-dismissable flash-' . $message[0] . '">' . $message[1] . "</div></li>\n";
    }
    echo '</ul>';
}
?>
<div id="details_wrapper" ng-controller='PlanController'>
<div class="row-fluid" >
<!--   <div ng-controller='ResultController'>
<input ng-model='user.from'>
<input ng-model='user.to'>
<input ng-model='user.budget'>
<p>Travelling from {{user.from}}, to {{user.to}}  on a budget of {{user.budget}}</p>
</div>-->

</div>
    <div class="col-lg-12" ng-controller="MyCtrl">
        <div class="row">
<!--            <form action="<?php echo $access_url; ?>plan/" method="post">
        
        
            <div class="col-sm-10 well">
                From 
                <input type="hidden"  name="from[name]" value="{{user.from.name}}" class="form-control" > 
                <input type="hidden"  name="from[lat]" value="{{user.from.lat}}" class="form-control" >
                <input type="hidden"  name="from[lng]" value="{{user.from.lng}}" class="form-control" >
               
    <input ng-model="choseFrom" googleplace id='input_from'/>
            
                To 
                <input type="hidden"  name="to[name]" value="{{user.to.name}}" class="form-control" > 
                <input type="hidden"  name="to[lat]" value="{{user.to.lat}}" class="form-control" >
                <input type="hidden"  name="to[lng]" value="{{user.to.lng}}" class="form-control" >
                <input ng-model="choseTo" googleplace id='input_to'/>
            
                On a Budget of {{cuurency_symbol}}<input type="text"  name="user.budget" value="" id="budget" class="" >
               
                
                <button type="submit" class="btn btn-success">Plan My Travel»</button>
                </div>
            </div>
         
            
        </div>    
</form>-->

            
            <div class="well well-sm center">
                <h2>Travelling from <span ng-bind="user.from.name"></span> to <span ng-bind="user.to.name"></span>  on a budget of <span ng-bind="currency_symbol"></span> <span ng-bind="user.budget"></span>  </h2>
            </div>
            
      </div>
<div class="row">
    <div class="col-lg-9" >
        
    

        <div class="row" ng-controller='TravelController'>
        <div class="col-xs-12 col-md-12">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-12 col-md-4 text-center">
                        <h3><i class="fa fa-plane fa-2x"></i>Get there </h3><h2 class="rating-num"><span ng-bind="currency_symbol"></span><span ng-bind="suggested.price |  number:0"></span> 
</h2>
                        
                        <div>Route <span ng-bind="suggested.name"></span>  (Cheapest)<br></div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="row rating-desc">
                            <div ng-repeat='route in routes ' >
                                <span class="col-lg-8">
                                     <span class=""><span ng-bind="route.name"></span></span> 
                                </span>
<!--                                <span class="col-lg-4">
                                     {{currency_symbol}}{{route.price  | number:0}}
                                </span>-->
                                <div class="col-lg-4"> 
                                 <div class="progress">
                                    <div style="width: {{ percent(route.price) | number:0 }}%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only1"><span ng-bind="currency_symbol"></span><span ng-bind="route.price  | number:0"></span></span>
                                    </div>
                                </div>
                                </div>
                               
                           
                                                       
                            
                            </div>
                           
                        </div>
                        
                        <!-- end row -->
                    </div>
                    
                   <div class="col-md-2 col-xs-12 text-center" style="vertical-alignment:middle"> <button class="btn btn-primary pull-right" type="button">
                
                Details</button></div>
            </div>
                </div> 
               
        </div>
        
    </div>
<div class="row" ng-controller='StayController'>
        <div class="col-xs-12 col-md-12">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-12 col-md-4 text-center">
                        <h3><i class="fa fa-home fa-2x"></i>Stay there </h3><h2 class="rating-num"><span ng-bind="currency_symbol"></span><span ng-bind="suggested.price |  number:0 "></span>
</h2>
                        
                        <div>Hotel <span ng-bind="suggested.name"></span> (Cheapest)<br></div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="row rating-desc">
                            <div ng-repeat='hotel in hotels ' >
                                <span class="col-lg-8">
                                     <span class=""><i class="fa fa-star "></i><span ng-bind="hotel.tripAdvisorRating|number:0"></span> &nbsp;<span ng-bind="hotel.name"></span></span>
                                </span>
                               
                                <div class="col-lg-4"> 
                                 <div class="progress">
                                    <div style="width: {{ percent(hotel.lowRate) | number:0 }}%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                       <span class="sr-only1"><span ng-bind="currency_symbol"></span><span ng-bind="hotel.lowRate  | number:0"></span></span>
                                    </div>
                                </div>
                                </div>
                               
                           
                                                       
                            
                            </div>
                           
                        </div>
                        
                        <!-- end row -->
                    </div>
                    
                   <div class="col-md-2 col-xs-12 text-center" style="vertical-alignment:middle"> <button class="btn btn-primary pull-right" type="button">
                
                Details</button></div>
            </div>
                </div> 
               
        </div>
        
    </div>

<div class="row">
        <div class="col-xs-12 col-md-12">
           <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-12 col-md-4 text-center">
                        <h3><i class="fa fa-glass fa-2x"></i>Explore there </h3><h2 class="rating-num">Rs 300</h2>
                        
                        <div>Choosing budget activities<br></div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="row rating-desc">
                            <div class="col-xs-3 col-md-3 text-right">Mount abu<br></div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress ">
                                    <div style="width: 80%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">Rs 2000</span>
                                    </div>
                                </div>
                            </div>
                            <!-- end 5 -->
                            <div class="col-xs-3 col-md-3 text-right">Train Square<br></div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress">
                                    <div style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">Rs 300</span>
                                    </div>
                                </div>
                            </div>
                            <!-- end 4 -->
                            <div class="col-xs-3 col-md-3 text-right">Cathedral<br></div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress">
                                    <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">40%</span>
                                    </div>
                                </div>
                            </div>
                            <!-- end 3 -->
                            <div class="col-xs-3 col-md-3 text-right">Dream Sale<br></div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress">
                                    <div style="width: 20%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">20%</span>
                                    </div>
                                </div>
                            </div>
                            <!-- end 2 -->
                            <div class="col-xs-3 col-md-3 text-right">Conference</div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress">
                                    <div style="width: 15%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">15%</span>
                                    </div>
                                </div>
                            </div>
                            <!-- end 1 -->
                        </div>
                        <!-- end row -->
                    </div>
                     <div class="col-md-2 col-xs-12 text-center" style="vertical-alignment:middle"> <button class="btn btn-primary pull-right" type="button">
                
                Details</button></div>
            
                </div>
            </div>
        </div>
    </div>
    </div>
    
    
    
    <div class="col-lg-3">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-check fa-4x"></i>
                    </div>
                    <div class="col-xs-10 text-right">
                        <p class="announcement-heading"><span ng-bind="currency_symbol"></span><span ng-bind="user.budget"></span></p>
                        <p class="announcement-text">Total expense <span ng-bind="calculateBudget()"></p>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            Budget 
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-tasks fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <p class="announcement-heading">8</p>
                        <p class="announcement-text">Places covered</p>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            Fix Issues
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-comments fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <p class="announcement-heading">12</p>
                        <p class="announcement-text">People also going there!</p>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            Complete Orders
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div><!-- /.row -->

 	<div class="row">

	
        <div class="col-md-12">
    		<div class="thumbnail center well well-small text-center">
                <h2> <span>Get this plan to your email.</span></h2>
                
                
                
                <form action="" method="post">
                    <div class="input-prepend"><span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" id="" name="" placeholder="your@email.com"> <input type="submit" value="Send Now!" class="btn btn-large" />
                    </div>
                    
                    
              </form>
            </div>    
        </div>
	</div>



</div>
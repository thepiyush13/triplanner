<?php
$flashMessages = Yii::app()->user->getFlashes();
if ($flashMessages) {
    echo '<ul class="flashes">';
    foreach($flashMessages as $key => $message) {
        echo '<li><div class="alert alert-dismissable flash-' . $message[0] . '">' . $message[1]. "</div></li>\n";
    }
    echo '</ul>';
}
?>

<script type='text/javascript'>
<?php
$php_array = $server_data;
$js_array = json_encode($php_array, JSON_NUMERIC_CHECK);
echo "server_data = " . $js_array . ";\n";
?>
    // console.log(server_data);
</script>


<div class="container-fluid" ng-controller="PlanController"  ng-app="myApp" >
    <div class="row-fluid">
        <div class="span1">
            <div class="sidebar-nav">
                <ul class="nav nav-list">
                    <li class="nav-header">Home</li>
                    <li class="active"><a href="#">Plan</a></li>
                    <li><a href="#">Travel</a></li>
                    <li><a href="#">Stay</a></li>
                    <li><a href="#">Explore</a></li>
                     <li><a href="#">Connect</a></li>
<!--                    <li class="nav-header">Sidebar</li>
                    <li><a href="#">Link</a></li>
                    <li><a href="#">Link</a></li>
                    <li><a href="#">Link</a></li>
                    <li><a href="#">Link</a></li>
                    <li><a href="#">Link</a></li>
                    <li><a href="#">Link</a></li>
                    <li class="nav-header">Sidebar</li>
                    <li><a href="#">Link</a></li>
                    <li><a href="#">Link</a></li>
                    <li><a href="#">Link</a></li>-->
                </ul>
            </div><!--/.well -->
        </div><!--/span-->

        <div class="span11">
            <div class="well well-small">

                <div class="row-fluid show-grid">
                    <div class="span12 text-center">
                        
                        <form action="/plan/" method="post">

                            <div class="row-fluid" ng-controller="SearchController" > <div class="row-fluid"><div class="span12"><div class="row-fluid show-grid"><p>
                                            </p>
                                            <div class="row-fluid">
                                                <div class="span4">
                                                   
<!--                                                    <input type="hidden" name="from[name]"  ng-model="user.client.from.name" class="form-control"> 
                                                    <input type="hidden" name="from[lat]" value="{{user.client.from.lat}}" class="form-control">
                                                    <input type="hidden" name="from[lng]" value="{{user.client.from.lng}}" class="form-control">-->

                                                    <input ng-model="choseFrom" googleplace id="input_from" class="input-block-level" placeholder=" Travelling From {{userData.from.name}}">
                                                </div>

                                                <div class="span3">
                                                    
<!--                                                    <input type="hidden" name="to[name]" value="{{user.client.to.name}}" class="form-control"> 
                                                    <input type="hidden" name="to[lat]" value="{{user.client.to.lat}}" class="form-control">
                                                    <input type="hidden" name="to[lng]" value="{{user.client.to.lng}}" class="form-control">-->
                                                    <input ng-model="choseTo" googleplace id="input_to" class="input-block-level" placeholder="To {{userData.to.name}}">
                                                </div>
<div class="span4">
    <div class="row">
        <div class="span3" ><span >Your Budget </span></div>
        <div class="progress span6" style="height: 20px;">
              <div class="bar" style="width:{{percent(userData.budget,userData.suggestedBudget) }}%;"></div>              
        </div>
        <div class="span2">  {{planData.currency_symbol}}{{userData.budget | number:0}}</div>
    </div>
     <div class="row">
        <div class="span3" ><span >Suggested Budget </span></div>
        <div class="progress span6" style="height: 20px;">
              <div class="bar" style="width:{{percent(userData.suggestedBudget,userData.budget) }}%;"></div>              
        </div>
        <div class="span2">  {{planData.currency_symbol}}{{userData.suggestedBudget | number:0}}</div>
    </div>
</div>
                                                <div class="span3">
                                                    
                                                    
                                                   On a Budget of {{planData.currency_symbol}} <input placeholder="" type="text" name="budget" ng-model="userData.budget" id="budget" class="input-small">
                                                </div>
                                                <div class="span4">
                                                    <label class="checkbox inline">
                                    <input type="checkbox" id="inlineCheckbox1" checked value="CHEAP_TRAVEL" >Affordable Travel
                                </label>
                                <label class="checkbox inline">
                                    <input type="checkbox" id="inlineCheckbox2" value="FUN_TRAVEL" ng-model="userData.filter.mainFilter.FUN_TRAVEL" > Fun Travel
                                </label>
                                <label class="checkbox inline">
                                    <input type="checkbox" id="inlineCheckbox3" value="BUSINESS_TRAVEL" ng-model="userData.filter.mainFilter.BUSINESS_TRAVEL" > Business Travel
                                </label>
                                
                            </div>
                                                <div class="span4">
                                                     
                                                    <button type="button" class="btn btn-success btn-small" ng-click='search()'>Plan My Travel»</button>
                                                </div>
                                            </div>


                                        </div>

                                    </div></div>

                            </div>

                        </form>
                       
                        
                    </div>
                </div>
<!--THE AJAX LOADER-->
<div class="well text-center lead" id="ajax_loader" style="display:none"><i class="fa fa-spinner fa-spin fa-2x card-heading-highlight"></i> Please Wait, Getting best deals for You......</div>
            </div>

            <div class="row-fluid">
                <div class="span6" ng-controller="TravelController">
                    <div class="card"><ul class="nav nav-pills pull-right">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">More</a></li>

                        </ul>
                        <h1 class="card-heading simple card-heading-highlight"><i class="fa fa-plane fa-2x card-heading-highlight"></i><small>Travel at {{planData.currency_symbol}}{{suggested.price}}</small></h1>
                        <p class="well well-small text-medium  text-center">
                             <i class="fa fa-thumbs-up"></i>   Good Deal:
Route 
{{suggested.name}}
will cost about
{{planData.currency_symbol}}{{suggested.price}} 
                            </p>
                        
                       <div  class="card-body card-listing" ng-controller="TravelTableController" id="travel_table" >

                          
<button ng-click="tableParams.reload()" class="btn pull-right">Reload</button>
    <table ng-table="tableParams" show-filter="false" class="table ng-table-responsive" id="travel_table">
        <tr ng-repeat="route in $data" ng-class="{ 'text-success': route.price == suggested.price }">
          
            <td sortable="route.name" data-title="'Name'">
                {{route.name}}
            </td>
            <td sortable="route.distance" data-title="'Distance(Km)'" 
                
                >
                {{route.distance}}
            </td>
            <td sortable="route.duration" data-title="'Duration(Hrs)'">
                {{route.duration}}
            </td>
            <td sortable="route.price" data-title="'Price'">
                {{planData.currency_symbol}}{{route.price |number :0}}
            </td>
        </tr>
    </table>    
<!--    <script type="text/ng-template" id="ng-table/filters/age.html">
        <input type="radio" ng-model="params.filter()[name]" name="filter-age" value="" /> None
        <br />
        <input type="radio" ng-model="params.filter()[name]" name="filter-age" value="50" /> 50 years
    </script>-->
                       </div>
   
<!--<button ng-click="tableParams.sorting({})" class="btn btn-default pull-right">Clear sorting</button>-->
<a class="btn" href="#">Cheapest</a><a class="btn" href="#">Fastest</a><a class="btn" href="#">Shortest</a><a class="btn pull-right" href="#">details »</a>
</div>
                    </div> 
                        
                    
                <!--/span-->
                <div class="span6" ng-controller="StayController">
                    <div class="card"><ul class="nav nav-pills pull-right">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">More</a></li>

                        </ul>
                        <h1 class="card-heading simple card-heading-highlight"><i class="fa fa-home fa-2x card-heading-highlight"></i><small>Stay at {{planData.currency_symbol}}{{suggested.price|number :0}}</small></h1>
                        <p class="well well-small text-medium  text-center">
                                <i class="fa fa-thumbs-up"></i> Good Deal:
Hotel 
{{suggested.name}}
will cost about
{{planData.currency_symbol}}{{suggested.price|number :0}} 
                            </p>


                        
                       <div  class="card-body card-listing" ng-controller="StayTableController" id="stay_table">

                            
    

    <table ng-table="tableParams" show-filter="false" class="table ng-table-responsive">
        <tr ng-repeat="hotel in $data" ng-class="{ 'text-success': hotel.lowRate == suggested.price }">
          <td data-title="'Image'">
                
              <img src="{{imgBaseUrl}}{{hotel.thumbNailUrl }}" class="img-thumb" />
            </td>
            <td sortable="hotel.name" data-title="'Name'">
                
                {{hotel.name}}
            </td>
            <td sortable="hotel.tripAdvisorRating" data-title="'Rating'" 
                
                >
                {{hotel.tripAdvisorRating}}
            </td>
            <td sortable="duration" data-title="'Available'">
                {{hotel.RoomRateDetailsList.RoomRateDetails.propertyAvailable}}
            </td>
            <td sortable="price" data-title="'Max Price'">
                {{planData.currency_symbol}}{{hotel.highRate | number :0}}
            </td>
            <td sortable="price" data-title="'Min Price'">
                {{planData.currency_symbol}}{{hotel.lowRate|number :0}}
            </td>
        </tr>
    </table>    
    
                       </div>
   
<!--<button ng-click="tableParams.sorting({})" class="btn btn-default pull-right">Clear sorting</button>-->
<a class="btn" href="#">Cheapest</a><a class="btn" href="#">Fastest</a><a class="btn" href="#">Shortest</a><a class="btn pull-right" href="#">details »</a>

                    </div>
                </div><!--/span-->
                <!--/span-->
            </div><!--/row-->
            <div class="row-fluid">
              <div class="span6" ng-controller="ExploreController">
                    <div class="card"><ul class="nav nav-pills pull-right">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">More</a></li>

                        </ul>
                        <h1 class="card-heading simple card-heading-highlight"><i class="fa fa-globe fa-2x card-heading-highlight"></i>
                            <small> Explore  {{suggested.rating||'NA'}} <i class="fa fa-star"></i>rated cafe</small></h1>
                        <p class="well well-small text-medium  text-center">
                                <i class="fa fa-thumbs-up"></i> Good Deal:
Cafe 
{{suggested.name}}
has awesome coffee
& is rated {{(suggested.rating)|| 'NA'}} / 5
                            </p>


                        
                       <div  class="card-body card-listing" ng-controller="ExploreTableController">

                            
    

    <table ng-table="tableParams" show-filter="false" class="table ng-table-responsive">
        <tr ng-repeat="place in $data" ng-class="{ 'text-success': place.rating == suggested.rating }">
          <td data-title="'Image'">
                
              <img src="{{place.image_url}}" class="img-thumb" />
            </td>
            <td sortable="place.name" data-title="'Name'">
                
                {{place.name}}
            </td>
            <td  data-title="'Rating'"  >
                <img src="{{place.rating_img_url_small}}" class="rating-small" />
                
            </td>
            <td  data-title="'Type'">
                {{place.categories.join(', ')}}
            </td>
             <td data-title="'Address'">
                 {{place.location.address.join('| ')}}
            </td>
            <td data-title="'Details'">
                <a href="{{place.url}}" target="_blank">Details</a>
            </td>
           
        </tr>
    </table>    
    
                       </div>
   
<!--<button ng-click="tableParams.sorting({})" class="btn btn-default pull-right">Clear sorting</button>-->
<a class="btn" href="#">Cheapest</a><a class="btn" href="#">Fastest</a><a class="btn" href="#">Shortest</a><a class="btn pull-right" href="#">details »</a>

                    </div>
                </div><!--/span-->
                <div class="span6">
                    <div class="card"><ul class="nav nav-pills pull-right">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">More</a></li>

                        </ul>
                        <h2 class=""></h2><h1 class="card-heading simple page-header card-heading-highlight"><i class="fa fa-group fa-2x card-heading-highlight"></i><small>Connect with Friends <small> Meet fellow travellers</small></h1>
                        <div class="card-body card-listing">
                            <div class="">

         <div id="card-downloads" class="card">
            <div class="card-heading image">
               
               <div class="card-heading-header">
                  <h3>Connect with Facebook or Google</h3>
                  <span>Get personalized recommendation and connect with fellow travellers</span>
               </div>
            </div>
            <div class="card-actions text-center">
               <a class="btn  btn-large btn-primary" href="#">Connect with facebook</a>
               
               <a class="btn  btn-large" href="#">
                  Connect with Google
                  <i class="icon-github"></i>
               </a>
               <p class="lead"> Stay tuned, Coming Soon......</p>
            </div>
         </div>

        

      </div>
                           
                        </div>
                    </div>
                </div><!--/span-->
                <!--/span-->
            </div><!--/row-->
           <div class="well well-sm text-center"><div class="btn-group">
                <button class="btn btn-large">Download Plan</button>
                <button class="btn btn-large dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
              </div>

                <p></p>

            </div>
        </div><!--/span-->
    </div><!--/row-->




</div>

 

    <style>
    .ng-table tr.emphasis td {
        background-color: #DDD;
        font-weight: bold;
    }
    </style>
<!--PHP PART-->

<?php $access_url = 'index.php?r=' ?>

<!-- Main hero unit for a primary marketing message or call to action -->

</div>


<?php
$flashMessages = Yii::app()->user->getFlashes();
if ($flashMessages) {
    echo '<ul class="flashes">';
    foreach ($flashMessages as $key => $message) {
        echo '<li><div class="alert alert-dismissable flash-' . $key . '">' . $message . "</div></li>\n";
    }
    echo '</ul>';
}
?>


<!--END PHP PART-->



<script type='text/javascript'>
<?php
//$php_array = $server_data;
//$js_array = json_encode($php_array, JSON_NUMERIC_CHECK);
//echo "server_data = " . $js_array . ";\n";
?>
    // console.log(server_data);
</script>






<form action="<?php echo $access_url; ?>plan/" method="post" ng-controller="IndexController">

    <div class="card" ng-controller="MyIndexCtrl" ng-app="myApp"> <div class="row-fluid"><div class="span8 offset2"><div class="row-fluid show-grid"><p>
                    </p>
                    <div class="row-fluid"><h1><i class="fa fa-globe fa-1x card-heading-highlight"></i>
                            Plan Your travel,Within Your Budget
                            <i class="fa fa-mail-reply-all fa-2x card-heading-highlight"></i>
                        </h1><br/> </div>
                    <div class="row-fluid well well-large text-center lead"><div class="span4">
                            From 
                            <input type="hidden" name="from[name]" value="{{userData.from.name}}" class="form-control"> 
                            <input type="hidden" name="from[lat]" value="{{userData.from.lat}}" class="form-control">
                            <input type="hidden" name="from[lng]" value="{{userData.from.lng}}" class="form-control">

                            <div><input ng-model="choseFrom" googleplace id="input_from" class="input-block-level"></div>
                        </div>

                        <div class="span4">
                            To 
                            <input type="hidden" name="to[name]" value="{{userData.to.name}}" class="form-control"> 
                            <input type="hidden" name="to[lat]" value="{{userData.to.lat}}" class="form-control">
                            <input type="hidden" name="to[lng]" value="{{userData.to.lng}}" class="form-control">
                            <div><input ng-model="choseTo" googleplace id="input_to" class="input-block-level"></div>
                        </div>

                        <div class="span4">
                            On a Budget of 

                            $ <div><input type="text" name="budget" value="" id="budget"  class="input-block-level"></div>
                        </div>
                    </div>


                </div><div class="row-fluid show-grid">

                    <div class="span4 offset3">

                        <div><button type="submit" class="btn btn-success btn-large btn-block">Plan My Travel»</button></div>
                    </div>
                </div>
            </div><div class="row"> <br></div><br></div>

    </div>

</form>

<div class="row-fluid">


    <div class="span4">

        <div class="card">
            <h3 class="card-heading simple"><i class="fa fa-credit-card fa-2x card-heading-highlight"></i>&nbsp;Travel on a budget</h3>
            <div class="card-body">
                <p>I am travelling alone, Any tips for keeping costs down?
                </p>When you're on your own, lodging becomes the main,
                annoying expense.Keep it down by this smart planner to get cheapest place possible in any city.Find solo cruisers discounts , deals and lots more to make your travel easy and affordable<p>
                </p></div>
            <div class="card-actions">
                <button class="btn btn-block btn-default">View Details</button>
            </div>
        </div>

    </div>
    <div class="span4">

        <div class="card">
            <h3 class="card-heading simple"><i class="fa fa-road fa-2x card-heading-highlight"></i>&nbsp;Explore new destinations</h3>
            <div class="card-body">
                <p>I'm planning to take a trip by myself. Are there any destinations that are especially appealing for solo travelers?
                </p>No destination is strictly off-limits to solo travelers, but some places are easier (and more appealing) to navigate than others.We will tell you best places in town or nearby.<p>
                </p></div>
            <div class="card-actions">
                <button class="btn btn-block btn-default">View Details</button>
            </div>
        </div>

    </div>
    <div class="span4">

        <div class="card">
            <h3 class="card-heading simple"><i class="fa fa-group fa-2x card-heading-highlight"></i>&nbsp;Connect with fellow Travellers</h3>
            <div class="card-body">
                <p>What are some ways I can connect with others on the road?
                </p>You'll want to start planting the seed before you go: Talk up your travel plans on this site. You might be surprised to discover a long-lost friend from college who plans on passing through Paris when you are, too.<p>
                </p></div>
            <div class="card-actions">
                <button class="btn btn-block btn-default">View Details</button>
            </div>
        </div>

    </div>

</div>


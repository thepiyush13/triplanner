
<?php $access_url = 'index.php?r=' ?>

<!-- Main hero unit for a primary marketing message or call to action -->

</div>


<?php
$flashMessages = Yii::app()->user->getFlashes();
if ($flashMessages) {
    echo '<ul class="flashes">';
    foreach($flashMessages as $key => $message) {
        echo '<li><div class="alert alert-dismissable flash-' . $key . '">' . $message. "</div></li>\n";
    }
    echo '</ul>';
}
?>






      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="jumbotron" ng-controller="MyCtrl">
    <div class="container">
        <h1>Plan Your travel,Within Your Budget</h1>
        <br> 
    <form action="<?php echo $access_url; ?>plan/" method="post">
        
        <div class="row col-lg-offset-1" >
            <div class="col-sm-3 col-md-3">
                From 
                <input type="hidden"  name="from[name]" value="{{user.from.name}}" class="form-control" > 
                <input type="hidden"  name="from[lat]" value="{{user.from.lat}}" class="form-control" >
                <input type="hidden"  name="from[lng]" value="{{user.from.lng}}" class="form-control" >
               
    <div><input ng-model="choseFrom" googleplace id='input_from'/></div>
            </div>
            <div class="col-sm-3 col-md-3">
                To 
                <input type="hidden"  name="to[name]" value="{{user.to.name}}" class="form-control" > 
                <input type="hidden"  name="to[lat]" value="{{user.to.lat}}" class="form-control" >
                <input type="hidden"  name="to[lng]" value="{{user.to.lng}}" class="form-control" >
                <div><input ng-model="choseTo" googleplace id='input_to'/></div>
            </div>
            <div class="col-sm-4 col-md-4">
                
                On a Budget of 
                
              <?php echo yii::app()->session['CURRENCY'] ?> </span><input type="text" name="budget" value="" id="budget" class="input-lg" >
                </div>
            </div>
            
            
       
        
        <div class="row">
            <br />
            <div class="col-lg-offset-4 col-md-4">
                <p>
              <button type="submit" class="btn btn-success btn-lg btn-block">Plan My Travel»</button>
            </p>
                <!--<p class="center"><input type="submit" class="btn btn-success btn-lg" value="Plan My Travel»"></p>-->
            </div>
            <br>

        </div>
</form>
</div>
      </div>


      <div class="container">

<div class="col-sm-4 col-md-4">

            <div class="card">
               <h3 class="card-heading simple">Travel on a budget</h3>
               <div class="card-body">
                  <p>I am travelling alone, Any tips for keeping costs down?
                  </p><p>
               </p></div>
               <div class="card-actions">
                  <button class="btn btn-block btn-default">View Details</button>
               </div>
            </div>

       </div>
        <div class="col-sm-4 col-md-4">

            <div class="card">
               <h3 class="card-heading simple">Explore new destinations</h3>
               <div class="card-body">
                  <p>I'm planning to take a trip by myself. Are there any destinations that are especially appealing for solo travelers?
                  </p><p>
               </p></div>
               <div class="card-actions">
                  <button class="btn btn-block btn-default">View Details</button>
               </div>
            </div>

       </div>
        <div class="col-sm-4 col-md-4">

            <div class="card">
               <h3 class="card-heading simple">Connect</h3>
               <div class="card-body">
                  <p>What are some ways I can connect with others on the road?
                  </p><p>
               </p></div>
               <div class="card-actions">
                  <button class="btn btn-block btn-default">View Details</button>
               </div>
            </div>

        </div>
      
      
      </div>
      <!-- Example row of columns -->
      
            

        </div>
      

      <footer>
         
        <p>© Company 2013</p>
      </footer>

      </div> <!-- /container -->

      <!-- Le javascript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
<!--      <script src="http://www.rome2rio.com/api/1.2/Autocomplete.js" type="text/javascript"></script>
<script type="text/javascript">
    var options = {};
    var autocomplete1 = new r2r.Autocomplete("r2r_from", options);
    var autocomplete2 = new r2r.Autocomplete("r2r_to", options);
</script>-->